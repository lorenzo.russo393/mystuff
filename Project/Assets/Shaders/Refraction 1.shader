Shader "Custom/Refraction"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Strength ("Strength", Range(0, 1)) = 0
        _Transparency ("Transparency", Range(0, 1)) = 0
        _Smoothness ("Smoothness", Range(0, 1)) = 0
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _FresnelColor("Fresnel color", Color) = (1,1,1,1)
        [PowerSlider(4)] _FresnelPower("Fresnel power", Range(0.5, 4)) = 0.5
        _RefractIndex("Index of Refraction", Range(-1, 1)) = 0
    }
    SubShader
    {
        Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }

        // Grab the screen behind the object into _BackgroundTexture
        GrabPass 
        {
            "_GrabBackground"
        }

        CGPROGRAM
    // Upgrade NOTE: excluded shader from DX11; has structs without semantics (struct v2f members grabPos)
    //#pragma exclude_renderers d3d11
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows alpha vert:vertex


        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _GrabBackground;

        struct Input
        {
            float2 uv_MainTex;
            float3 viewDir;
            float3 worldPos;
            float4 screenPos;
            float4 grabPos;

        };

        half _Metallic;
        half _Smoothness;
        fixed4 _Color;
        float _Transparency;
        float _Strength;
        float3 _FresnelColor;
        float _FresnelPower;
        float _RefractIndex;

        void vert(inout appdata_full v, Input i) 
        {
            float3 worldSpaceNormal = UnityObjectToWorldNormal(v.normal);
            float objectViewDir = WorldSpaceViewDir(v.vertex);

            float3 refraction = refract(normalize(objectViewDir), normalize(worldSpaceNormal), _RefractIndex);

            float3 objectSpaceRefract = mul(unity_WorldToObject, refraction);

            float4 clipPos = UnityObjectToClipPos(objectSpaceRefract);

            i.grabPos = ComputeGrabScreenPos(clipPos);
        }

        void surf (Input i, inout SurfaceOutputStandard o)
        {
            float4 color = tex2Dproj(_GrabBackground, i.grabPos);

            // Albedo comes from a texture tinted by color
            //fixed4 c = tex2D (_MainTex, i.uv_MainTex) * _Color;
            /*float3 worldSpaceNormal = normalize(cross(ddx(i.worldPos), ddy(i.worldPos)));
            float3 dirRefraction = refract(normalize(i.viewDir), normalize(worldSpaceNormal), _RefractIndex);

            float2 grabTexture =  i.screenPos.xy / i.screenPos.w;
            float3 offset = i.screenPos + dirRefraction;
            
            fixed4 backgroundTexture = tex2D(_GrabBackground, grabTexture * float4(offset.x, offset.y, 0, 0));

            */
            float fresnelBackground = 1.0 - saturate(dot(normalize(i.viewDir), o.Normal));
            fresnelBackground = pow(fresnelBackground, _FresnelPower);
            float3 fresnelCol = fresnelBackground * _FresnelColor;

            o.Emission = fresnelCol;
            o.Albedo = color * _Transparency + _Color;
            o.Metallic = _Metallic;
            o.Smoothness = _Smoothness;
            o.Alpha = _Strength;
        }
        ENDCG
    }
    FallBack "Standard"
}
