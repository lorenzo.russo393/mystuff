using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogueSystem : MonoBehaviour
{
    public Dialogue dialogue;
    private Queue<string> dialogues;
    private int index;

    [Header("Texts components")]
    [SerializeField] TextMeshProUGUI nameText;
    [SerializeField] TextMeshProUGUI dialogueText;

    private void Start()
    {
        dialogues = new Queue<string>();
    }

    private void Update()
    {
        TriggerText();
    }

    private void TriggerText()
    {
        if(Input.GetMouseButtonDown(0))
        {
            AssignTexts();
        }
    }

    private void AssignTexts()
    {
        nameText.text = dialogue.npc_name;
        
        foreach(string text in dialogue.dialogues)
        {
            dialogues.Enqueue(text);
            
        }

        NextText();
        index++;
    }

    private void NextText()
    {
        if (index == dialogue.dialogues.Length)
        {
            dialogueText.text = "";
            nameText.text = "";

            index = 0;
            dialogues.Clear();
            dialogues.Dequeue();
        }
        else
        {
            StopAllCoroutines();
            StartCoroutine(LetterByLetter(dialogues.Dequeue()));
        }
    }

    private IEnumerator LetterByLetter(string text)
    {
        dialogueText.text = "";

        foreach(char letter in text.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }      
    }
}
