using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[System.Serializable]
public class Dialogue 
{
    public string npc_name;
    [TextArea(2, 2)]
    public string[] dialogues;
}
