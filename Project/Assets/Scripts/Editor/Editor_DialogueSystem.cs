using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DialogueSystem))]
public class Editor_DialogueSystem : Editor
{
    SerializedProperty stringsArray;

    private void OnEnable()
    {
       
    }

    /*public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        serializedObject.Update();

        CreateDialogueArray();
    }
    */

    private void CreateDialogueArray()
    {
        if (GUILayout.Button("Create dialogue", GUILayout.Width(100)))
        {
            stringsArray = serializedObject.FindProperty("dialogue");
            EditorGUILayout.PropertyField(stringsArray);
        }
    }
}
